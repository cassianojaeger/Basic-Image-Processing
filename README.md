# Basic Image Processing Software  #

### Basic Information ###

This implementation is part of Image Processing algorithms during my FPI (Image Process Fundamentals) classes in college.
It contains:
* Image rotation 
* Image flip
* Negative
* Apply filters (Gaussian, LaPlace, Prewitt...)
* And more features which you can see using the software.

The software was developed in Java. The front end part was designed and coded using Java Swing framework.

<div style="display:inline-block;vertical-align:top;">
	<img src="fpi_trab.png?raw=true" alt="Drawing" width="600"/> 
</div>