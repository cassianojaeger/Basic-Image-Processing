/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imagemanipulation;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Arrays;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Cassiano
 */
public class ImageMethods {
    //VARIAVEIS DA CLASSE{
    private int numberOfShades = 0;
    private BufferedImage originalImage = null;
    private BufferedImage newImage = null;
    public BufferedImage auxImage = null;
    private float contrastLevel;
    private File imagefile = null;
    private String nomeImagemEntrada = null;
    private String nomeImagemSaida = null;
    private int brightnessLevel;
    public int arrayOfshadesOfGray[] = new int[256];
    public int histogramArray[] = new int[256];
    public int cumHistogram[] = new int[256];
    //}
    private Object g;
    
    public float getContrastLevel() {
        return contrastLevel;
    }
    public void setContrastLevel(float contrastLevel) {
        this.contrastLevel = contrastLevel;
    }
    public int getBrightnessLevel() {
        return brightnessLevel;
    }
    public void setBrightnessLevel(int brightnessLevel) {
        this.brightnessLevel = brightnessLevel;
    }
    public BufferedImage getOriginalImage() {
        return originalImage;
    }
    public void setOriginalImage(BufferedImage originalImage) {
        this.originalImage = originalImage;
    }
    public int getNumberOfShades() {
        return numberOfShades;
    }
    public void setNumberOfShades(int numberOfShades) {
        this.numberOfShades = numberOfShades;
    }
    public BufferedImage getNewImage() {
        return newImage;
    }
    public void setNewImage(BufferedImage originalImage) {
        this.newImage = originalImage;
    }
    public File getImagefile() {
        return imagefile;
    }
    public void setImagefile(File imagefile) {
        this.imagefile = imagefile;
    }
    public String getNomeImagemEntrada() {
        return nomeImagemEntrada;
    }
    public void setNomeImagemEntrada(String nomeImagemEntrada) {
        this.nomeImagemEntrada = nomeImagemEntrada;
    }
    public String getNomeImagemSaida() {
        return nomeImagemSaida;
    }
    public void setNomeImagemSaida(String nomeImagemSaida) {
        this.nomeImagemSaida = nomeImagemSaida;
    }
    public boolean writeImage(){
        boolean erroLeitura;
         try {
            ImageIO.write(getNewImage(), "jpg",new File(getNomeImagemSaida()));
            return erroLeitura = false;
        }//BLOCO QUE VERIFICA ALGUM ERRO DE LEITURA//BLOCO QUE VERIFICA ALGUM ERRO DE LEITURA//BLOCO QUE VERIFICA ALGUM ERRO DE LEITURA//BLOCO QUE VERIFICA ALGUM ERRO DE LEITURA
            catch (IOException e) {
              return erroLeitura = true;       
        }
    }
    public void turnGrayScale(){
        //NOVA IMAGEM
        //ITERAÇÃO QUE VAI VARRER OS PIXELS DA IMAGEM E COLETAR SUAS CORES
        //CÓDIGO INSPIRADO EM PESQUISAS NA INTERNET
        for(int line = 0; line < getNewImage().getWidth(); line++){
            for(int column = 0; column< getNewImage().getHeight(); column++){
                Color c = new Color(getNewImage().getRGB(line , column));
                int redValue = c.getRed();
                int greenValue = c.getGreen();
                int blueValue = c.getBlue();
                
                int grayValue = (int) (0.299*redValue + 0.587*greenValue + 0.114*blueValue);
                Color gColor = new Color(grayValue, grayValue, grayValue, grayValue);
                getNewImage().setRGB(line, column, gColor.getRGB());
            }
        }
    }
    public void quantizationImage(){
        Arrays.fill(arrayOfshadesOfGray, 0);
        //CALCULA VETOR DE MEDIANAS DE TONS DE CINZA
        calculateMidValuesOfGray();
        //FAZ O HISTOGRAMA
        for(int column = 0; column < getNewImage().getWidth(); column++){
            for(int line = 0; line< getNewImage().getHeight(); line++){
               Color c = new Color(getNewImage().getRGB(column, line)); 
               //for que verifica qual dos intervalos o pixel atual se encontra
               for(int rangeOfGray = 1; rangeOfGray <= getNumberOfShades(); rangeOfGray++){
                    if(c.getRed()< (rangeOfGray*256)/getNumberOfShades()){
                        Color gColor = new Color(arrayOfshadesOfGray[rangeOfGray], arrayOfshadesOfGray[rangeOfGray], arrayOfshadesOfGray[rangeOfGray], c.getAlpha());
                        getNewImage().setRGB(column, line, gColor.getRGB());
                        //GAMBIARRA QUE SAI DO FOR MAIS INTERIOR
                        rangeOfGray = getNumberOfShades() +1;
                    }
               }
            }
        }   
    }
    public void flipHorizontally(){
        int auxFlip =1;
        int columnAux;
        for(int column = 0; column < (getNewImage().getWidth())/2; column++){
            for(int line = 0; line< getNewImage().getHeight(); line++){
               columnAux = getNewImage().getWidth()-auxFlip;
               Color c = new Color(getNewImage().getRGB(column, line));
               Color c1 = new Color(getNewImage().getRGB(columnAux, line));
               
               getNewImage().setRGB(column, line, c1.getRGB());
               getNewImage().setRGB(columnAux, line, c.getRGB());
               
            }
        auxFlip++;    
        }
    }
    public void flipVertically(){
        int auxFlip =1;
        int lineAux;
         for(int column = 0; column < getNewImage().getWidth(); column++){
            for(int line = 0; line< (getNewImage().getHeight())/2; line++){
               lineAux = getNewImage().getHeight()-auxFlip;
               Color c = new Color(getNewImage().getRGB(column, line));
               Color c1 = new Color(getNewImage().getRGB(column, lineAux));
               
               getNewImage().setRGB(column, line, c1.getRGB());
               getNewImage().setRGB(column, lineAux, c.getRGB());
               auxFlip++;   
            }
        auxFlip = 1; 
        }
    }
    public void manipulateBrightness(){
         for(int line = 0; line < getNewImage().getWidth(); line++){
            for(int column = 0; column< getNewImage().getHeight(); column++){
                Color c = new Color(getNewImage().getRGB(line , column));
                int redValue = c.getRed()+getBrightnessLevel();
                int greenValue = c.getGreen()+getBrightnessLevel();
                int blueValue = c.getBlue()+getBrightnessLevel();

                if (redValue >= 256) {
                    redValue = 255;
                } else if (redValue < 0) {
                    redValue = 0;
                }

                if (greenValue >= 256) {
                    greenValue = 255;
                } else if (greenValue < 0) {
                    greenValue = 0;
                }

                if (blueValue >= 256) {
                    blueValue = 255;
                } else if (blueValue < 0) {
                    blueValue = 0;
                }
                
                Color gColor = new Color(redValue, greenValue, blueValue);
                getNewImage().setRGB(line, column, gColor.getRGB());
            }
        }
    }
    public void manipulateContrast(){
        for(int line = 0; line < getNewImage().getWidth(); line++){
            for(int column = 0; column< getNewImage().getHeight(); column++){
                Color c = new Color(getNewImage().getRGB(line , column));
                int redValue =   (int) (c.getRed()*getContrastLevel());
                int greenValue = (int) (c.getGreen()*getContrastLevel());
                int blueValue =  (int) (c.getBlue()*getContrastLevel());

                if (redValue >= 256) {
                    redValue = 255;
                } 
                if (greenValue >= 256) {
                    greenValue = 255;
                }
                if (blueValue >= 256) {
                    blueValue = 255;
                } 
                Color gColor = new Color(redValue, greenValue, blueValue);
                getNewImage().setRGB(line, column, gColor.getRGB());
            }
        }
    }
    public void turnNegative(){
        for(int line = 0; line < getNewImage().getWidth(); line++){
            for(int column = 0; column< getNewImage().getHeight(); column++){
                Color c = new Color(getNewImage().getRGB(line , column));
                int redValue =   255 - c.getRed();
                int greenValue = 255 - c.getGreen();
                int blueValue =  255 -c.getBlue();

                Color gColor = new Color(redValue, greenValue, blueValue);
                getNewImage().setRGB(line, column, gColor.getRGB());
            }
        }
    }
    
    public void createHistogram() {
             
        BufferedImage copyOfImage = new BufferedImage(getNewImage().getWidth(), getNewImage().getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics aux = copyOfImage.createGraphics();
        Graphics ori = getNewImage().createGraphics();
        aux.drawImage(getNewImage(), 0, 0, null);
        
        if(isColoredImage()){
            turnGrayScale();
        }
         Arrays.fill(histogramArray, 0);
         for(int line = 0; line < getNewImage().getWidth(); line++){
            for(int column = 0; column< getNewImage().getHeight(); column++){
                Color c = new Color(getNewImage().getRGB(line , column));
                histogramArray[c.getBlue()] = histogramArray[c.getBlue()] + 1; 
            }
        }
        ori.drawImage(copyOfImage, 0, 0, null);   
    }
    public void histogramEqualization(){
        createCumHistogram();
        normalizeCumHistogram();
        for(int line = 0; line < getNewImage().getWidth(); line++){
            for(int column = 0; column< getNewImage().getHeight(); column++){
                Color c = new Color(getNewImage().getRGB(line , column));
 
                int redValue = cumHistogram[c.getRed()];
                int greenValue = cumHistogram[c.getGreen()];
                int blueValue = cumHistogram[c.getBlue()];
                int alphaValue = c.getAlpha();

                Color gColor = new Color(redValue, greenValue, blueValue, alphaValue);
                getNewImage().setRGB(line, column, gColor.getRGB());
            }
        }
    }   
    public void zoomOut(int resizedX, int resizedY) {
        int newSizeX = (int)Math.ceil((double)getNewImage().getWidth()/resizedX);    
        int newSizeY = (int)Math.ceil((double)getNewImage().getHeight()/resizedY);
        BufferedImage resizedImage = new BufferedImage(newSizeX, newSizeY, BufferedImage.TYPE_INT_ARGB);
        int[][] pixelArrayOfOriginalImage = convertTo2DUsingGetRGB(getNewImage());

        int redValue,greenValue,blueValue,alphaValue;
        for(int countXAux = 0; countXAux < getNewImage().getWidth(); countXAux+=resizedX){
            for(int countYAux = 0;countYAux < getNewImage().getHeight(); countYAux+=resizedY){
                redValue = 0;blueValue = 0;greenValue = 0;alphaValue = 0;
                for(int line = countYAux;(line<countYAux+resizedY)&&(line<getNewImage().getHeight());line++){
                    for(int column = countXAux;column<(countXAux+resizedX)&&(column<getNewImage().getWidth());column++){
                        alphaValue += (pixelArrayOfOriginalImage[line][column] >> 24)& 0xFF;
                        redValue += (pixelArrayOfOriginalImage[line][column] >> 16) & 0xFF;
                        greenValue += (pixelArrayOfOriginalImage[line][column] >>8 ) & 0xFF;
                        blueValue += (pixelArrayOfOriginalImage[line][column]) & 0xFF;
                    }
                }
                alphaValue = alphaValue/(resizedX*resizedY);
                redValue = redValue/(resizedX*resizedY);
                blueValue = blueValue/(resizedX*resizedY);
                greenValue = greenValue/(resizedX*resizedY);
                Color gColor = new Color(redValue, greenValue, blueValue, alphaValue);
                resizedImage.setRGB((countXAux/resizedX),(countYAux/resizedY), gColor.getRGB());
            }
        }
        setNewImage(resizedImage);
    }
    
    public void ZoomIn(){
        int newSizeX = (getNewImage().getWidth()*2)-1;
        int newSizeY = (getNewImage().getHeight()*2)-1;
        BufferedImage resizedImage = new BufferedImage(newSizeX, newSizeY, BufferedImage.TYPE_INT_ARGB);
        int[][] arrayOfpixel = convertTo2DUsingGetRGB(getNewImage());
        int[][] resizedArray = new int [newSizeY][newSizeX];
        int redValue,greenValue,blueValue,alphaValue;
        //CRIAR MATRIZ DA IMAGEM AUMENTADA COM APENAS OS PIXELS ORIGINAIS AVALIADOS
        for(int line = 0; line < newSizeY; line+=2){
            for(int column = 0;column < newSizeX; column+=2){
                resizedArray[line][column] = arrayOfpixel[line/2][column/2];
                alphaValue = (resizedArray[line][column] >> 24)& 0xFF;
                redValue = (resizedArray[line][column] >> 16) & 0xFF;
                greenValue = (resizedArray[line][column] >>8 ) & 0xFF;
                blueValue = (resizedArray[line][column]) & 0xFF;
                Color gColor = new Color(redValue, greenValue, blueValue, alphaValue);
                resizedImage.setRGB(column, line, gColor.getRGB());
            }
        }
        
        //INTERPOLA AS COLUNAS DA IMAGEM AUMENTADA
       for(int line = 0; line < newSizeY; line+=2){
            for(int column = 0;column < newSizeX-1; column+=2){
                //RETIRA OS VALORES RGB E FAZ A INTERPOLAÇÃO
                alphaValue = (((resizedArray[line][column] >> 24)& 0xFF) + ((resizedArray[line][column+2] >> 24)& 0xFF))/2;
                redValue = (((resizedArray[line][column] >> 16)& 0xFF) + ((resizedArray[line][column+2] >> 16)& 0xFF))/2;
                greenValue = (((resizedArray[line][column] >> 8)& 0xFF) + ((resizedArray[line][column+2] >> 8)& 0xFF))/2;
                blueValue = ((resizedArray[line][column]& 0xFF) + (resizedArray[line][column+2]& 0xFF))/2;
                //POE OS VALORES NOS PIXELS CORRETOS DA IMAGEM
                Color gColor = new Color(redValue, greenValue, blueValue, alphaValue);
                resizedImage.setRGB(column+1, line, gColor.getRGB());
                //POE OS VALORES DO ARRAY SECUNDARIO
                resizedArray[line][column+1] = (alphaValue << 24) + (redValue<<16) + (greenValue<<8) + blueValue;
            }
        } 
       
       //INTERPOLA AS LINHAS DA IMAGEM
        for(int column = 0; column < newSizeX; column++){
            for(int line = 0;line < newSizeY-1; line+=2){
                //RETIRA OS VALORES RGB E FAZ A INTERPOLAÇÃO
                alphaValue = (((resizedArray[line][column] >> 24)& 0xFF) + ((resizedArray[line+2][column] >> 24)& 0xFF))/2;
                redValue = (((resizedArray[line][column] >> 16)& 0xFF) + ((resizedArray[line+2][column] >> 16)& 0xFF))/2;
                greenValue = (((resizedArray[line][column] >> 8)& 0xFF) + ((resizedArray[line+2][column] >> 8)& 0xFF))/2;
                blueValue = ((resizedArray[line][column]& 0xFF) + (resizedArray[line+2][column]& 0xFF))/2;
                //POE OS VALORES NOS PIXELS CORRETOS DA IMAGEM
                Color gColor = new Color(redValue, greenValue, blueValue, alphaValue);
                resizedImage.setRGB(column, line+1, gColor.getRGB());
            }
        }
        setNewImage(resizedImage);
    }
    
    //FUNÇÃO QUE GERA A IMAGEM ATUAL ROTACIONADA 90°HORARIO
    public void rotateClockWise(){
        int[][] arrayOfpixel = convertTo2DUsingGetRGB(getNewImage());
        int redValue,greenValue,blueValue,alphaValue;
        int[][] resizedArray = rotateMatrixClockwise(arrayOfpixel);     
        BufferedImage resizedImage = new BufferedImage(resizedArray[0].length, resizedArray.length, BufferedImage.TYPE_INT_ARGB);

        for(int line = 0; line < resizedArray.length; line++){
            for(int column = 0;column < resizedArray[0].length; column++){
                //RETIRA OS VALORES RGB E FAZ A INTERPOLAÇÃO
                alphaValue = (resizedArray[line][column] >> 24)& 0xFF;
                redValue = (resizedArray[line][column] >> 16) & 0xFF;
                greenValue = (resizedArray[line][column] >>8 ) & 0xFF;
                blueValue = (resizedArray[line][column]) & 0xFF;

                Color gColor = new Color(redValue, greenValue, blueValue, alphaValue);
                resizedImage.setRGB(column, line, gColor.getRGB());
            }
        }
        setNewImage(resizedImage);    
    }
    //FUNÇÃO QUE GERA A IMAGEM ATUAL ROTACIONADA 90°ANTI HORARIO
    public void rotateCounterClockWise(){
        int[][] arrayOfpixel = convertTo2DUsingGetRGB(getNewImage());
        int redValue,greenValue,blueValue,alphaValue;
        int[][] resizedArray = rotateMatrixCounterClockwise(arrayOfpixel);
           BufferedImage resizedImage = new BufferedImage(resizedArray[0].length, resizedArray.length, BufferedImage.TYPE_INT_ARGB);
           for(int line = 0; line < resizedArray.length; line++){
               for(int column = 0;column < resizedArray[0].length; column++){
                   //RETIRA OS VALORES RGB E FAZ A INTERPOLAÇÃO
                   alphaValue = (resizedArray[line][column] >> 24)& 0xFF;
                   redValue = (resizedArray[line][column] >> 16) & 0xFF;
                   greenValue = (resizedArray[line][column] >>8 ) & 0xFF;
                   blueValue = (resizedArray[line][column]) & 0xFF;

                   Color gColor = new Color(redValue, greenValue, blueValue, alphaValue);
                   resizedImage.setRGB(column, line, gColor.getRGB());
               }
           }
           setNewImage(resizedImage);
   }
    //FUNÇÃO QUE ROTA A MATRIZ 90°HORARIO  
    public float[][] setKernel(String kernelName){
       float[][] kernelMatrix = new float[3][3];
       if("Gaussian".equals(kernelName)){
           float[] arrayNumbers = new float[]{0.0625f, 0.125f, 0.0625f, 0.125f, 0.25f, 0.125f, 0.0625f, 0.125f, 0.0625f};
           kernelMatrix = populateMatrix(arrayNumbers);
       }else{
           if("Laplacian".equals(kernelName)){
               float[] arrayNumbers = new float[]{0f, -1f, 0f, -1f, 4f, -1f, 0f, -1f, 0f};
               kernelMatrix = populateMatrix(arrayNumbers);
           }else{
               if("High Pass".equals(kernelName)){
                    float[] arrayNumbers = new float[]{-1f, -1f, -1f, -1f, 8f, -1f, -1f, -1f, -1f};
                    kernelMatrix = populateMatrix(arrayNumbers);
               }else{
                   if("Prewitt Hx".equals(kernelName)){
                        float[] arrayNumbers = new float[]{-1f, 0f, 1f, -1f, 0f, 1f, -1f, 0f, 1f};
                        kernelMatrix = populateMatrix(arrayNumbers);
                   }else{
                       if("Prewitt Hy".equals(kernelName)){
                            float[] arrayNumbers = new float[]{-1f, -1f, -1f, 0f, 0f, 0f, 1f, 1f, 1f};
                            kernelMatrix = populateMatrix(arrayNumbers);
                       }else{
                            if("Sobel Hx".equals(kernelName)){
                                float[] arrayNumbers = new float[]{-1f, 0f, 1f, -2f, 0f, 2f, -1f, 0f, 1f};
                                kernelMatrix = populateMatrix(arrayNumbers);
                           }else{
                                if("Sobel Hy".equals(kernelName)){
                                    float[] arrayNumbers = new float[]{-1f, -2f, -1f, 0f, 0f, 0f, 1f, 2f, 1f};
                                    kernelMatrix = populateMatrix(arrayNumbers);
                                }else{
                                    if("Your Kernel".equals(kernelName)){
                                        float[] arrayNumbers = new float[9];
                                        kernelMatrix = populateMatrix(arrayNumbers);
                                    }
                                }
                            }
                       } 
                   }
               }
           }
       }       
       return kernelMatrix;
   } 
    //FUNÇÃO AUXILIAR PARA POPULAR A MATRIZ KERNEL
    // É ELA QUE INSERE OS VALORES DOS KERNELS NA MATRIZ
    private float[][] populateMatrix(float[] arrayNumbers){
       float[][] matrix = new float[3][3];
       int index =0;
       for(int i = 0; i<3;i++){
           for(int j =0; j<3;j++){
               matrix[i][j] = arrayNumbers[index];
               index++;
           }
       }
       return matrix;
   }
    private boolean isColoredImage(){
       for(int column = 0; column < getNewImage().getWidth(); column++){
            for(int line = 0; line< getNewImage().getHeight(); line++){
                Color c = new Color(getNewImage().getRGB(column , line));
                if(c.getRed() != c.getBlue() || c.getBlue() != c.getGreen() || c.getGreen() != c.getRed()){
                    return true;
                }
            }
        }
        return false;
    } 
    private void createCumHistogram(){
        Arrays.fill(cumHistogram, 0);
        cumHistogram[0] = 255 * histogramArray[0];
        for(int i = 1; i <= 255;i++){
            cumHistogram[i] = (int) (cumHistogram[i-1] + (255 * histogramArray[i]));
        }      
    }
    private void normalizeCumHistogram(){
        int maxPixels = (getNewImage().getHeight()*getNewImage().getWidth());
        for(int i = 0; i <= 255;i++){
            cumHistogram[i] = cumHistogram[i]/maxPixels ;
        }
    }
    private static int[][] rotateMatrixClockwise(int[][] matrix)
    {
        int width = matrix.length;
        int height = matrix[0].length;
        int[][] ret = new int[height][width];
        for (int line=0; line<height; line++) {
            for (int column=0; column<width; column++) {
                ret[line][column] = matrix[width - column - 1][line];
            }
        }
        return ret;
    }
    //FUNÇÃO QUE ROTA A MATRIZ 90°ANTI HORARIO
    private static int[][] rotateMatrixCounterClockwise(int[][] matrix)
    {
        int width = matrix.length;
        int height = matrix[0].length;   
        int[][] ret = new int[height][width];
        for (int line=0; line<height; line++) {
            for (int column=0; column<width; column++) {
                ret[line][column] = matrix[column][height - line - 1];
            }
        }
        return ret;
    }
    //FUNÇÃO QUE PEGA OS VALORES RGBS E CRIA UMA MATRIZ DOS VALORES
    private static int[][] convertTo2DUsingGetRGB(BufferedImage image) {
        //index foi uma gambiarra usada para fazer o Zoom In da imagem
      int width = (image.getWidth()); 
      int height = (image.getHeight());
      int[][] result = new int[height][width];

      for (int row = 0; row < height; row++) { //aqui faz a duplicação do tamanho da matriz no eixo y
         for (int col = 0; col < width; col++) {//aqui faz a duplicação do tamanho da matriz no eixo x
            result[row][col] = image.getRGB(col, row);
         }
      }
      return result;
   }
    //FUNÇÃO QUE POPULA O KERNEL COM OS VALORES PRE-ESTABELECIDOS PELO USUÁRIO
    private void calculateMidValuesOfGray(){
        int medValRange;
        int inicRange = 0;
        int endingRange;
            for(int n = 1; n <= getNumberOfShades(); n++){
                endingRange = inicRange + 256/getNumberOfShades();
                medValRange = (inicRange + endingRange)/2;
                arrayOfshadesOfGray[n] = medValRange;
                inicRange = endingRange;
            }
    }
    public void convolution(float[][] kernelMatrix,String kernelName){
        BufferedImage copyOfImage = new BufferedImage(getNewImage().getWidth(), getNewImage().getHeight(), BufferedImage.TYPE_INT_ARGB);
        int[][] arrayOfpixel = convertTo2DUsingGetRGB(getNewImage());
        int grayValue;   
        kernelMatrix = rotateKernelClockwise(kernelMatrix);//MÉTODO ROTATE REUSADO PARA FLOAT, POIS NÃO É POSSIVEL FAZER CAST DE ARRAY INT PARA FLOAT EM JAVA
        kernelMatrix = rotateKernelClockwise(kernelMatrix);//ROTADO 180GRAUS     
        
        for (int line=1; line<arrayOfpixel.length-1; line++) {
            for (int column=1; column<arrayOfpixel[0].length-1; column++) {
                grayValue = 
                  (int) ((arrayOfpixel[line-1][column-1]&0xFF)*kernelMatrix[0][0] +(arrayOfpixel[line-1][column]&0xFF)*kernelMatrix[0][1]
                        +(arrayOfpixel[line-1][column+1]&0xFF)*kernelMatrix[0][2] + (arrayOfpixel[line][column-1]&0xFF)*kernelMatrix[1][0] 
                        +(arrayOfpixel[line][column]&0xFF)*kernelMatrix[1][1] + (arrayOfpixel[line][column+1]&0xFF)*kernelMatrix[1][2]
                        +(arrayOfpixel[line+1][column-1]&0xFF)*kernelMatrix[2][0] + (arrayOfpixel[line+1][column]&0xFF)*kernelMatrix[2][1] 
                        +(arrayOfpixel[line+1][column+1]&0xFF)*kernelMatrix[2][2]);
                //POE OS VALORES NOS PIXELS CORRETOS DA IMAGEM
                 if(kernelName.equals("Prewitt Hx")||kernelName.equals("Prewitt Hy")||kernelName.equals("Sobel Hx")||kernelName.equals("Sobel Hy")){
                    grayValue += 127;
                }
                if (grayValue >= 256) {
                    grayValue = 255;
                } else if (grayValue < 0) {
                    grayValue = 0;
                }
               
                Color gColor = new Color(grayValue, grayValue, grayValue, grayValue);
                copyOfImage.setRGB(column, line, gColor.getRGB());
            }
        }
        setNewImage(copyOfImage);
    }
    private static float[][] rotateKernelClockwise(float[][] matrix)
    {
        int width = matrix.length;
        int height = matrix[0].length;
        float[][] ret = new float[height][width];
        for (int line=0; line<height; line++) {
            for (int column=0; column<width; column++) {
                ret[line][column] = matrix[width - column - 1][line];
            }
        }
        return ret;
    }
    
    
    //NOTEI QUE SE EU USAR A TURNGRAYSCALE ALGUMAS VEZES, ELA CORRIGE OS VALORES DE CINZA ATE UM CERTO PONTO.
}
 